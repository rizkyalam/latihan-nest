// dto = data transfer object

export interface UserDTO {
  username: string;
  password: string;
  email: string;
}
