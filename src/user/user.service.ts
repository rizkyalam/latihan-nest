import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserDTO } from './user.dto';
import { UserEntity } from './user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private UserRepository: Repository<UserEntity>,
  ) {}

  async getAll(): Promise<string> {
    return 'ini di service';
  }

  async showAll() {
    return await this.UserRepository.find();
  }

  async storeData(data: UserDTO) {
    const userdata = await this.UserRepository.create(data);
    await this.UserRepository.save(userdata);
    return userdata;
  }

  async getDetail(id: string) {
    return await this.UserRepository.findOne(id);
  }

  async updateData(id: string, data: Partial<UserDTO>) {
    await this.UserRepository.update(id, data);
    return await this.UserRepository.findOne({
      where: { id },
    });
  }

  async deleteData(id: string) {
    await this.UserRepository.delete(id);
    return { delete: true };
  }
}
