import {
  Body,
  Controller,
  Delete,
  forwardRef,
  Get,
  Inject,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { UserService } from './user.service';
import { UserDTO } from './user.dto';

@Controller('user')
export class UserController {
  constructor(
    @Inject(forwardRef(() => UserService))
    private UserService: UserService,
  ) {}

  @Get()
  index(): string {
    return 'testing controller';
  }

  @Post()
  store(@Body() data: UserDTO) {
    return this.UserService.storeData(data);
  }

  @Get('data')
  showAll() {
    return this.UserService.showAll();
  }

  @Get('all')
  getAll(): Promise<string> {
    return this.UserService.getAll();
  }

  @Get(':id')
  getDetail(
    @Param('id')
    id: string,
  ) {
    return this.UserService.getDetail(id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() data: Partial<UserDTO>) {
    return this.UserService.updateData(id, data);
  }

  @Delete(':id')
  destroy(@Param('id') id: string) {
    return this.UserService.deleteData(id);
  }
}
